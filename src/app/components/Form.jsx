"use client";
import React, { useState } from "react";
import { Input, Spinner, Button } from "@nextui-org/react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/navigation";
import { auth } from "../store/firebase";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";
import {
  getFirestore,
  collection,
  addDoc,
  query,
  where,
  getDocs,
  doc,
  setDoc,
} from "firebase/firestore";
import { login } from "../redux/auth/auth";

export default function Form({
  title,
  buttonText,
  signText,
  signLink,
  signHref,
}) {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();

  const registerHandler = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const userCredential = await createUserWithEmailAndPassword(
        auth,
        email,
        password
      );
      const user = userCredential.user;

      const userData = {
        username: username,
        email: email,
        bio: "",
        city: "",
        total_score: 0,
        social_media: "",
      };

      const db = getFirestore();
      const userDocRef = doc(db, "users", user.uid);
      await setDoc(userDocRef, userData);
      dispatch(login());
      router.push("/home");
    } catch (error) {
      setError("Registration failed. Please check your information.");
    } finally {
      setLoading(false);
    }
  };

  const loginHandler = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      await signInWithEmailAndPassword(auth, email, password);
      router.push("/home");
      dispatch(login());
    } catch (error) {
      setError("Login failed. Please check your email and password.");
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="w-full bg-white rounded-lg shadow dark:shadow md:mt-0 sm:max-w-md xl:p-0 dark:bg-black dark:shadow-gray-950">
      <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
          {title}
        </h1>
        {error && <div className="text-red-500">{error}</div>}

        <form
          className="space-y-4 md:space-y-6"
          onSubmit={title === "Sign Up" ? registerHandler : loginHandler}
        >
          {title === "Sign Up" && (
            <Input
              type="username"
              label="Username"
              name="username"
              id="username"
              labelPlacement="outside"
              className="bg-gray-50 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-transparent dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="username"
              isRequired
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          )}
          <Input
            type="email"
            label="Email Address"
            name="email"
            id="email"
            labelPlacement="outside"
            className="bg-gray-50 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-transparent dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="name@company.com"
            isRequired
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Input
            type="password"
            label="Password"
            name="password"
            id="password"
            labelPlacement="outside"
            className="bg-gray-50 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-transparent dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="your password"
            isRequired
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          {title === "Sign In" && (
            <div className="flex items-center justify-between">
              <a
                href="/forgot-password"
                className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500"
              >
                Forgot password?
              </a>
            </div>
          )}

          <Button
            color="primary"
            isLoading={loading}
            onClick={(e) => {
              e.preventDefault();
              title === "Sign Up" ? registerHandler(e) : loginHandler(e);
            }}
          >
            {buttonText}
          </Button>

          <p className="text-sm font-light text-gray-500 dark:text-gray-400">
            {signText}
            <a
              href={signHref}
              className="font-medium text-primary-600 hover:underline dark:text-primary-500"
            >
              {signLink}
            </a>
          </p>
        </form>
      </div>
    </div>
  );
}
