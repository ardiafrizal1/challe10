import Form from "@/app/components/Form";
import React from "react";

export default function Page() {
  return (
    <Form
      title="Sign Up"
      buttonText="Sign Up"
      signText="Already have an account? "
      signLink="Sign In"
      signHref="/login"
    />
  );
}
