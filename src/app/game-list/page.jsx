"use client";
import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Image,
  Button,
  Link,
} from "@nextui-org/react";

export default function Page() {
  return (
    <>
      <div className="container ml-64 mt-10 flex gap-2">
        <Link href="game-list" aria-current="page">
          Game List
        </Link>
      </div>
      <div className="container mx-auto">
        <div className=" mt-10 bg-white shadow-lg py-16 rounded-lg dark:bg-black dark:shadow-md dark:shadow-black animate__animated animate__fadeInUp">
          <div className="flex items-center justify-center flex-wrap animate__animated animate__fadeInUp">
            <div className="w-full flex flex-col justify-center mb-6 animate__animated animate__fadeInUp">
              <p className="text-center text-4xl font-bold mb-3">Game List</p>
              <p className="text-center text-xl font-semibold">
                Here&lsquo;s all game that you can play!
              </p>
            </div>
            <div className="md:mb-3 animate__animated animate__fadeInUp">
              <Card
                isFooterBlurred
                className="w-full h-[300px] col-span-12 sm:col-span-5 md:mb-3"
              >
                <CardHeader className="absolute z-10 top-1 flex-col items-start"></CardHeader>
                <Image
                  removeWrapper
                  alt="Card example background"
                  className="z-0 w-full h-full scale-125 -translate-y-6 object-cover"
                  src="/images/roullete.avif"
                />
                <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-center">
                  <div>
                    <p className="text-black text-tiny">Available soon.</p>
                  </div>
                </CardFooter>
              </Card>
            </div>
            <div className="mx-4 md:mb-3 animate__animated animate__fadeInUp animate__delay-1s">
              <Card
                isFooterBlurred
                className="w-full h-[300px] col-span-12 sm:col-span-5"
              >
                <CardHeader className="absolute z-10 top-1 flex-col items-start"></CardHeader>
                <Image
                  removeWrapper
                  alt="Card example background"
                  className="z-0 w-full h-full scale-125 -translate-y-6 object-cover"
                  src="/images/rps.avif"
                />
                <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-evenly">
                  <Button
                    className="text-tiny"
                    color="primary"
                    radius="full"
                    size="sm"
                    href="/game-play"
                    as={Link}
                  >
                    Play
                  </Button>
                  <Button
                    className="text-tiny"
                    color="primary"
                    radius="full"
                    size="sm"
                    href="/game-detail"
                    as={Link}
                  >
                    Detail Game
                  </Button>
                </CardFooter>
              </Card>
            </div>
            <div className="md:mb-3 animate__animated animate__fadeInUp animate__delay-2s">
              <Card
                isFooterBlurred
                className="w-full h-[300px] col-span-12 sm:col-span-5"
              >
                <CardHeader className="absolute z-10 top-1 flex-col items-start"></CardHeader>
                <Image
                  removeWrapper
                  alt="Card example background"
                  className="z-0 w-full h-full scale-125 -translate-y-6 object-cover"
                  src="/images/vr.avif"
                />
                <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-center">
                  <div>
                    <p className="text-black text-tiny">Available soon.</p>
                  </div>
                </CardFooter>
              </Card>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
